grammar Music;

song
   : (element)* EOF
   ;

element
   : tempo
   | volume
   | octave
   | repeat
   | note
   | pause
   | chord
   ;

tempo
   : (tempoplus | tempominus | tempoabsolute)
   ;

tempoplus
   : ATPLUS INTEGER
   ;

tempominus
   : ATMINUS INTEGER
   ;

tempoabsolute
   : (AT | ATEQ) (INTEGER)?
   ;

volume
   : (volumeabsolut | volumeplus | volumeminus)
   ;

volumeabsolut
   : PERCENT (INTEGER)?
   ;

volumeplus
   : CLOSECR (INTEGER)?
   ;

volumeminus
   : OPENCR (INTEGER)?
   ;

octave
   : (octaveplus | octaveminus | octavehigh | octavemedium | octavelow)
   ;

octaveplus
   : PLUS
   ;

octaveminus
   : MINUS
   ;

octavelow
   : BACKSLASH
   ;

octavemedium
   : EQ
   ;

octavehigh
   : SLASH
   ;

repeat
   : OPENBR end += repeatcontent repeatendingnumber* repeatendingdefault? repeatendingnumber* CLOSEBR INTEGER?
   ;

repeatcontent
   : (element)*
   ;

repeatendingnumber
   : BAR INTEGER repeatcontent
   ;

repeatendingdefault
   : EXCLAMATION repeatcontent
   ;

chord
   : OPENSQ chordelements CLOSESQ chordduration?
   ;

chordelements
   : (note | tempo | volume | octave)*
   ;

note
   : NOTELETTER noteModifier
   ;

noteModifier
   : HASH? PERIOD? UNDERSCORE? duration?
   ;

pause
   : P duration?
   ;

chordduration
   : INTEGER
   | DOLLAR
   ;

duration
   : INTEGER
   ;

WS
   : (' ' | '\t' | '\f'
   // handle newlines
   | ('\r\n' // DOS/Windows
   | '\r' // Macintosh
   | '\n' // Unix
   )) -> channel (HIDDEN)
   ;

ML_COMMENT
   : '<' (ML_COMMENT | .)*? '>' -> channel (HIDDEN)
   ;

NOTELETTER
   : ('a' .. 'g')
   | ('A' .. 'G')
   ;

P
   : 'p'
   ;

MINUS
   : '-'
   ;

UNDERSCORE
   : '_'
   ;

EXCLAMATION
   : '!'
   ;

HASH
   : '#'
   ;

BAR
   : '|'
   ;

PERIOD
   : '.'
   ;

DOLLAR
   : '$'
   ;

OPENBR
   : '('
   ;

CLOSEBR
   : ')'
   ;

OPENCR
   : '{'
   ;

CLOSECR
   : '}'
   ;

OPENSQ
   : '['
   ;

CLOSESQ
   : ']'
   ;

AT
   : '@'
   ;

ATEQ
   : '@' '='
   ;

ATPLUS
   : '@' '+'
   ;

ATMINUS
   : '@' '-'
   ;

PERCENT
   : '%'
   ;

PLUS
   : '+'
   ;

EQ
   : '='
   ;

SLASH
   : '/'
   ;

BACKSLASH
   : '\\'
   ;

fragment DIGIT
   : ('0' .. '9')
   ;

INTEGER
   : (DIGIT)+
   ;

