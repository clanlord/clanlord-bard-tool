package net.heberling.clanlord.bard.player;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import net.heberling.clanlord.bard.parser.MusicLexer;
import net.heberling.clanlord.bard.parser.MusicParser;
import net.heberling.clanlord.bard.player.visitor.MusicVisitor;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;

public class MusicQueue {
  private TreeSet<Note> ts = new TreeSet<>();

  private Set<Integer> longChords = new HashSet<>();

  private int nextNotePosition = 0;
  private int nextChordPosition = 0;

  private final int melodyChannel;
  private final int chordChannel;

  private final InstrumentParameter instrument;

  /**
   * @param melodyChannel
   * @param chordChannel
   */
  public MusicQueue(InstrumentParameter instrument, int melodyChannel, int chordChannel) {
    super();
    this.instrument = instrument;
    this.melodyChannel = melodyChannel;
    this.chordChannel = chordChannel;
  }

  public void parse(String s, int channel, int tempo, int volume) throws MusicException {
    MusicLexer musicLexer = new MusicLexer(CharStreams.fromString(s));
    musicLexer.addErrorListener(new BailErrorListener());

    MusicParser musicParser = new MusicParser(new CommonTokenStream(musicLexer));
    musicParser.addErrorListener(new BailErrorListener());

    try {
      MusicParser.SongContext song_return = musicParser.song();

      new MusicVisitor(this, tempo, volume).visit(song_return);
    } catch (BailErrorListener.BailException e) {
      throw new MusicException(channel, e.getMessage(), e.getStartIndex());
    } catch (RecognitionException e) {
      throw new MusicException(
          channel,
          musicParser.getErrorHeader(e) + " " + musicParser.getErrorHeader(e),
          e.getOffendingToken().getStartIndex());
    } catch (RuntimeException e) {
      RecognitionException e2 = (RecognitionException) e.getCause();
      throw new MusicException(
          channel,
          musicParser.getErrorHeader(e2) + " " + musicParser.getErrorHeader(e2),
          e2.getOffendingToken().getStartIndex());
    }
  }

  public int getMelodyChannel() {
    return melodyChannel;
  }

  public InstrumentParameter getInstrument() {
    return instrument;
  }

  public void addAllFromMusicQueue(MusicQueue q) {
    Note[] n = q.getNotes();
    ts.addAll(Arrays.asList(n));
  }

  public synchronized void addNote(int note, int vel, int noteLenght, int nextNote) {
    // add note start
    ts.add(new Note(nextNotePosition, note, vel, melodyChannel, instrument));
    ts.add(new Note(nextNotePosition + noteLenght, note, 0, melodyChannel, instrument));

    nextNotePosition += nextNote;
  }

  public synchronized void addPause(int duration) {
    nextNotePosition += duration;
  }

  public synchronized void addChord(
      int[] chord, int lenght, int vel, int chordLenght, int nextChord) {
    // if(nextChordPosition<nextNotePosition)
    nextChordPosition = nextNotePosition;

    if (nextChord > 0) {
      // add note start
      for (int i = 0; i < lenght; i++)
        ts.add(new Note(nextChordPosition, chord[i], vel, chordChannel, instrument));
      // add note end
      for (int i = 0; i < lenght; i++)
        ts.add(new Note(nextChordPosition + chordLenght, chord[i], 0, chordChannel, instrument));
    } else {
      // it's a long chord, if we have seen it for a note stop, else start it
      for (int i = 0; i < lenght; i++) {
        if (longChords.contains(chord[i])) {
          ts.add(new Note(nextChordPosition, chord[i], 0, chordChannel, instrument));
          longChords.remove(chord[i]);
        } else {
          ts.add(new Note(nextChordPosition, chord[i], vel, chordChannel, instrument));
          longChords.add(chord[i]);
        }
      }
    }

    nextChordPosition += nextChord;
  }

  public Note[] getNotes() {
    Note[] n = new Note[ts.size()];

    ts.toArray(n);

    return n;
  }
}
