package net.heberling.clanlord.bard.player;

public class Note implements Comparable<Note> {
  public int position;

  public int note;

  public int vel;

  public int channel;

  public InstrumentParameter instrument;

  /**
   * @param position
   * @param noteOffset
   * @param vel
   * @param channel
   * @param instrument
   */
  public Note(int position, int noteOffset, int vel, int channel, InstrumentParameter instrument) {
    super();
    this.position = position;
    this.note = instrument.offsetToNote(noteOffset);
    this.vel = vel;
    this.channel = channel;
    this.instrument = instrument;
  }

  public int compareTo(Note n) {
    int ret = this.position - n.position;

    // stop notes before starting
    if (ret == 0) ret = this.vel == 0 ? -1 : 1;

    return ret;
  }
}
