package net.heberling.clanlord.bard.player;

public class MusicException extends Exception {

  private final int position;

  private final int channel;

  public MusicException(int channel, String error, int position) {
    super(error);
    this.channel = channel;
    this.position = position;
  }

  public String getError() {
    return getMessage();
  }

  public int getPosition() {
    return position;
  }

  public int getChannel() {
    return channel;
  }
}
