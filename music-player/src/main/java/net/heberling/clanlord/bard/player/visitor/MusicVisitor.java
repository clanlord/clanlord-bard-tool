package net.heberling.clanlord.bard.player.visitor;

import java.util.List;
import net.heberling.clanlord.bard.parser.MusicParser;
import net.heberling.clanlord.bard.player.InstrumentParameter;
import net.heberling.clanlord.bard.player.MusicConstants;
import net.heberling.clanlord.bard.player.MusicException;
import net.heberling.clanlord.bard.player.MusicQueue;
import org.antlr.v4.runtime.tree.ParseTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MusicVisitor implements MusicConstants {
  private static final Logger LOGGER = LoggerFactory.getLogger(MusicVisitor.class);
  private static final int DURATION_BLACK = 2;

  private static final int DURATION_WHITE = 4;

  /** MIDI values for ABCDEFG */
  private static final short[] MIDI_NOTES = {69, 71, 60, 62, 64, 65, 67};

  private final MusicQueue musicQueue;

  private int octave;

  private float pauseLength;

  private long noteLength;

  private int melodyVelocity;

  private int melodyVolume;

  private int tempo;

  private int velocity;

  private int chordVelocity;

  private final InstrumentParameter instrument;

  private Chord chord;

  private void setTempo(int inTempo) {
    LOGGER.debug("Tempo: {}", inTempo);
    tempo = inTempo;
    if (tempo > TEMPO_MAX) tempo = TEMPO_MAX;
    if (tempo < TEMPO_MIN) tempo = TEMPO_MAX;
    pauseLength = (1000 / (((float) tempo) / 60)) / 4; // changed 600
    // to 1000,
    // seems to be
    // correct speed
    // now
    LOGGER.debug("Pause lenght: {}", pauseLength);
    noteLength = ((long) pauseLength * 90) / 100;
  }

  private void setVolume(int inVelocity) {
    velocity = inVelocity;
    chordVelocity = (velocity * instrument.getChordVelocity()) / 100;
    melodyVelocity = (velocity * instrument.getMelodyVelocity()) / 100;
  }

  public MusicVisitor(MusicQueue musicQueue, int tempo, int volume) {
    super();
    this.musicQueue = musicQueue;
    this.instrument = musicQueue.getInstrument();
    setTempo((int) ((TEMPO_DEFAULT / 100f) * tempo));
    setVolume((int) ((VOLUME_DEFAULT / 100f) * volume));

    melodyVolume = 10;
  }

  //
  // Handle a note character
  //
  private Note getNote(char c, int octave) {
    Note mNote = null;
    if (c >= 'a' && c <= 'g') {
      mNote = new Note(MIDI_NOTES[c - 'a'] + (octave * OCTAVE), DURATION_BLACK);
    } else if (c >= 'A' && c <= 'G') {
      mNote = new Note(MIDI_NOTES[c - 'A'] + (octave * OCTAVE), DURATION_WHITE);
    } else return null;

    mNote.tone -= MIDDLE_C;

    return mNote;
  }

  int getNoteDuration(boolean inLinked, int inDuration) {
    return (int)
        (inLinked
            ? inDuration * pauseLength
            :
            // linked
            // not
            // has
            // no
            // small
            // rest
            (inDuration > 1)
                ? // else it has one
                ((inDuration - 1) * pauseLength) + noteLength
                : noteLength);
  }

  private void modifyNote(Note note, MusicParser.NoteModifierContext modifierTree) {
    if (modifierTree.duration() != null) {
      note.duration = Integer.parseInt(modifierTree.duration().getText());
    }
    if (modifierTree.UNDERSCORE() != null) {
      note.linked = true;
    }
    if (modifierTree.PERIOD() != null) {
      note.tone--;
    }
    if (modifierTree.HASH() != null) {
      note.tone++;
    }
  }

  public void visit(ParseTree node) throws MusicException {
    if (node == null) return;

    if (node instanceof MusicParser.NoteContext) {
      handleNote((MusicParser.NoteContext) node);
    } else if (node instanceof MusicParser.TempoContext) {
      handleTempo((MusicParser.TempoContext) node);
    } else if (node instanceof MusicParser.OctaveContext) {
      handleOctave((MusicParser.OctaveContext) node);
    } else if (node instanceof MusicParser.PauseContext) {
      handlePause((MusicParser.PauseContext) node);
    } else if (node instanceof MusicParser.VolumeContext) {
      handleVolume((MusicParser.VolumeContext) node);
    } else if (node instanceof MusicParser.RepeatContext) {
      handleLoop((MusicParser.RepeatContext) node);
    } else if (node instanceof MusicParser.ChordContext) {
      handleChord((MusicParser.ChordContext) node);
    } else {
      for (int i = 0; i < node.getChildCount(); i++) {
        visit(node.getChild(i));
      }
    }
  }

  private void handleChord(MusicParser.ChordContext node) throws MusicException {
    chord = new Chord();

    chord.volume = 10;

    chord.duration = 4;

    // duration has been specified
    if (node.chordduration() != null) {
      if (node.chordduration().DOLLAR() != null)
        if (instrument.isLongChordsSupported()) chord.duration = 0;
        else
          throw new MusicException(
              musicQueue.getMelodyChannel(),
              "no long chords",
              node.getStart().getCharPositionInLine());
      else chord.duration = Integer.parseInt(node.chordduration().INTEGER().getText());
    }

    // add notes
    visit(node.chordelements());

    musicQueue.addChord(
        chord.chord,
        chord.lenght,
        ((chordVelocity * (chord.volume * 10)) / 100),
        getNoteDuration(chord.linked, chord.duration),
        (int) (chord.duration * pauseLength));

    chord = null;
  }

  private void handleLoop(MusicParser.RepeatContext node) throws MusicException {
    int count = 2;

    // count has been specified
    if (node.INTEGER() != null) count = Integer.parseInt(node.INTEGER().getText());

    MusicParser.RepeatcontentContext content = node.repeatcontent(); // the notes to play

    // find endings
    List<MusicParser.RepeatendingnumberContext> endings = node.repeatendingnumber();
    MusicParser.RepeatendingdefaultContext defaultending = node.repeatendingdefault();

    for (int i = 1; i <= count; i++) {
      visit(content);
      int currentCount = i;
      MusicParser.RepeatcontentContext context =
          endings.stream()
              .filter(
                  repeatendingnumberContext ->
                      Integer.parseInt(repeatendingnumberContext.INTEGER().getText())
                          == currentCount)
              .map(MusicParser.RepeatendingnumberContext::repeatcontent)
              .findFirst()
              .orElse(null);

      if (context != null) {
        visit(context);
      } else if (defaultending != null) {
        visit(defaultending.repeatcontent());
      }
    }
  }

  private void handleVolume(MusicParser.VolumeContext node) {

    int vol = chord != null ? chord.volume : melodyVolume;

    if (node.volumeabsolut() != null) {
      vol =
          node.volumeabsolut().INTEGER() == null
              ? 10
              : Integer.parseInt(node.volumeabsolut().INTEGER().getText());
    } else if (node.volumeminus() != null) {
      vol =
          vol
              - (node.volumeminus().INTEGER() == null
                  ? 1
                  : Integer.parseInt(node.volumeminus().INTEGER().getText()));
    } else if (node.volumeplus() != null) {
      vol =
          vol
              + (node.volumeplus().INTEGER() == null
                  ? 1
                  : Integer.parseInt(node.volumeplus().INTEGER().getText()));
    }

    if (vol < 1) vol = 1;
    if (vol > 10) vol = 10;

    if (chord != null) chord.volume = vol;
    else melodyVolume = vol;
  }

  private void handlePause(MusicParser.PauseContext node) {
    int duration = DURATION_BLACK;

    if (node.duration() != null) {
      duration = Integer.parseInt(node.duration().getText());
    }

    musicQueue.addPause((int) (duration * pauseLength));
  }

  private void handleOctave(MusicParser.OctaveContext node) {
    if (node.octavelow() != null) {
      octave = -1;
    } else if (node.octavemedium() != null) {
      octave = 0;
    } else if (node.octavehigh() != null) {
      octave = 1;
    } else if (node.octaveminus() != null) {
      octave--;
    } else if (node.octaveplus() != null) {
      octave++;
    }

    if (octave > 1) octave = 1;
    else if (octave < -1) octave = -1;
  }

  private void handleTempo(MusicParser.TempoContext node) {
    if (node.tempoabsolute() != null) {
      setTempo(
          node.tempoabsolute().INTEGER() == null
              ? TEMPO_DEFAULT
              : Integer.parseInt(node.tempoabsolute().INTEGER().getText()));
    } else if (node.tempominus() != null) {
      setTempo(
          tempo
              - (node.tempominus().INTEGER() == null
                  ? 10
                  : Integer.parseInt(node.tempominus().INTEGER().getText())));
    } else if (node.tempoplus() != null) {
      setTempo(
          tempo
              + (node.tempoplus().INTEGER() == null
                  ? 10
                  : Integer.parseInt(node.tempoplus().INTEGER().getText())));
    }
  }

  private void handleNote(MusicParser.NoteContext node) {
    Note note = getNote(node.NOTELETTER().getText().charAt(0), octave);
    // process all modifiers
    modifyNote(note, node.noteModifier());

    if (chord != null) chord.addNote(note);
    else
      musicQueue.addNote(
          note.tone,
          (melodyVelocity * (melodyVolume * 10)) / 100,
          getNoteDuration(note.linked, note.duration),
          (int) (note.duration * pauseLength));
  }
}

class Note {
  int tone;

  int duration;

  boolean linked;

  public Note(int tone, int duration) {
    super();
    this.tone = tone;
    this.duration = duration;
  }
}

class Chord {
  int duration;
  int volume;
  boolean linked;
  int[] chord;
  int lenght;

  public Chord() {
    chord = new int[10];
  }

  void addNote(Note note) {
    chord[lenght] = note.tone;
    lenght++;
  }
}
