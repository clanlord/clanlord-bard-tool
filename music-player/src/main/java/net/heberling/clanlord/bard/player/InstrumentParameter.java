package net.heberling.clanlord.bard.player;

import java.io.DataInputStream;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** @author Atoro */
public class InstrumentParameter {
  private static final Logger LOGGER = LoggerFactory.getLogger(InstrumentParameter.class);
  public static final int INSTRUMENT_COUNT = 17;

  // initialize instruments
  private static final InstrumentParameter[] instrumentParameters =
      new InstrumentParameter[INSTRUMENT_COUNT + 1];

  public static final int FLAGS_NO_CHORDS = 0x01;

  public static final int FLAGS_NO_MELODY = 0x02;

  public static final int FLAGS_LONG_CHORD = 0x04;

  public static final int FLAGS_CUSTOM = 0x80;

  public static final int DEFAULT_POLYPHONY = 6;

  public static final int BITS_PER_LONG = Integer.SIZE; // sizeof(long)*8;

  private final int midiInstrument; // QuickTime instrument data;

  private final int mOctaveOffset; // Offset to kMiddleC in octave

  private int polyphony = 4, flags = 8;

  private final short chordVelocity; // in PERCENTS

  private final short melodyVelocity; // in PERCENTS

  private int[] mRestrictions;

  private final String name; // CL Instrument Name

  static {
    // load instruments from file
    try {
      DataInputStream dis =
          new DataInputStream(InstrumentParameter.class.getResourceAsStream("/CCLInstrument"));

      int i = 0;

      do {
        // quicktime ToneDescription
        int synthType = dis.readInt();

        String synthName = readStr31(dis);

        String instrName = readStr31(dis);

        int instrNumber = dis.readInt();
        int gmNumber = dis.readInt();

        // octave offset
        short octaveOffset = dis.readShort();

        byte unusedPolyphony = dis.readByte();
        byte flags = dis.readByte();
        int unused = (unusedPolyphony >> 4) & 0xF;
        int polyphony = (unusedPolyphony) & 0xF;

        short chordVelocity = dis.readShort();
        short melodyVelocity = dis.readShort();

        // SRestriction
        int[] restrictions =
            new int[((MusicConstants.NUM_OCTAVES * MusicConstants.OCTAVE) / BITS_PER_LONG) + 1];
        for (int j = 0; j < restrictions.length; j++) {
          restrictions[j] = dis.readInt();
        }

        // CL name
        String cclName = readStr31(dis);
        LOGGER.debug("CCLInstrument number:" + i);
        LOGGER.debug("Synthesizer type: " + synthType);
        LOGGER.debug("Synthesizer name: " + synthName);
        LOGGER.debug("Instrument Name: " + instrName);
        LOGGER.debug("Instrument Number: " + instrNumber);
        LOGGER.debug("GM Number " + gmNumber);
        LOGGER.debug("Octave offset :" + octaveOffset);

        LOGGER.debug("unused " + unused);
        LOGGER.debug("Polyphony " + polyphony);
        LOGGER.debug("Flags: " + flags);

        LOGGER.debug("Chord Velocity " + chordVelocity);
        LOGGER.debug("Melody Velovity: " + melodyVelocity);
        LOGGER.debug("CCLName: " + cclName);

        instrumentParameters[i] =
            new InstrumentParameter(
                cclName,
                gmNumber - 1,
                octaveOffset,
                polyphony,
                flags,
                chordVelocity,
                melodyVelocity,
                restrictions);

        i++;

      } while (i <= INSTRUMENT_COUNT);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static String readStr31(DataInputStream dis) throws IOException {
    int lenght = (dis.readByte() & 0xFF);
    byte[] b = new byte[lenght];

    dis.read(b);
    dis.skipBytes(31 - lenght);
    return new String(b, "x-MacRoman");
  }

  /**
   * @param name
   * @param midiInstrument
   * @param mOctaveOffset
   * @param polyphony
   * @param flags
   * @param chordVelocity
   * @param melodyVelocity
   * @param mRestrictions
   */
  private InstrumentParameter(
      String name,
      int midiInstrument,
      short mOctaveOffset,
      int polyphony,
      int flags,
      short chordVelocity,
      short melodyVelocity,
      int[] mRestrictions) {
    super();
    this.name = name;
    this.midiInstrument = midiInstrument;
    this.mOctaveOffset = mOctaveOffset + 1;
    this.polyphony = polyphony;
    this.flags = flags;
    this.chordVelocity = chordVelocity;
    this.melodyVelocity = melodyVelocity;
    this.mRestrictions = mRestrictions;
  }

  public static InstrumentParameter getInstrument(int instrument) {
    if (instrument >= 0 && instrument <= INSTRUMENT_COUNT) return instrumentParameters[instrument];
    else return null;
  }

  public static InstrumentParameter[] getAllInstrumentParameters() {
    return instrumentParameters;
  }

  int noteToOffset(int inNote) {
    return inNote
        - (MusicConstants.MIDDLE_C
            - MusicConstants.OCTAVE
            + (mOctaveOffset * MusicConstants.OCTAVE));
  }

  int offsetToNote(int inOffset) {
    return (MusicConstants.MIDDLE_C
            - MusicConstants.OCTAVE
            + (mOctaveOffset * MusicConstants.OCTAVE))
        + inOffset;
  }

  boolean hasMelody() {
    return (flags & FLAGS_NO_MELODY) == 0;
  }

  boolean hasChords() {
    return (flags & FLAGS_NO_CHORDS) == 0;
  }

  int getPolyphony() {
    return hasChords() ? polyphony : 0;
  }

  boolean isNoteRestricted(int inNote) {
    int off = noteToOffset(inNote);
    return isOffsetRestricted(off);
  }

  private boolean isOffsetRestricted(int inOffset) {
    return ((mRestrictions[inOffset / BITS_PER_LONG]
                >> (BITS_PER_LONG - 1 - (inOffset % BITS_PER_LONG)))
            & 1)
        != 0;
  }

  public boolean isLongChordsSupported() {
    return (flags & FLAGS_LONG_CHORD) > 0;
  }

  public int getMidiInstrument() {
    return midiInstrument;
  }

  public short getChordVelocity() {
    return chordVelocity;
  }

  public short getMelodyVelocity() {
    return melodyVelocity;
  }

  public String toString() {
    return name;
  }
}
