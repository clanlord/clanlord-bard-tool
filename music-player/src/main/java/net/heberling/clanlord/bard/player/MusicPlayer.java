package net.heberling.clanlord.bard.player;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.TreeMap;
import javax.sound.midi.Instrument;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Soundbank;
import javax.sound.midi.Synthesizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MusicPlayer implements Runnable {
  private static final Logger LOGGER = LoggerFactory.getLogger(MusicPlayer.class);
  private static MusicPlayer musicPlayer;

  private final MidiChannel[] channels;

  private final Synthesizer synth = null;

  private final HashSet<String> playerSet = new HashSet<>();

  private final TreeMap<String, Song> songMap = new TreeMap<>();

  private boolean running = false;

  private MusicQueue musicQueue;

  private Thread musicThread;

  private boolean useQuickTimeSoundbank = false;

  /** @param audioSynthesizer the Synthesizer to use. it must already be open! */
  private MusicPlayer(Synthesizer audioSynthesizer) {
    try {
      Synthesizer synth = audioSynthesizer;

      /*
       * No Synthesizer given, create a default one and open it..
       */
      if (synth == null) {
        synth = MidiSystem.getSynthesizer();
        synth.open();
      }
      LOGGER.debug("Synthesizer: " + synth);

      LOGGER.debug("Default soundbank: " + synth.getDefaultSoundbank());

      // try the macOs sound font
      File macOsSoundFont =
          new File(
              "/System/Library/Components/CoreAudio.component/Contents/Resources/gs_instruments.dls");
      if (macOsSoundFont.exists()) {
        LOGGER.debug("Trying to load " + macOsSoundFont);
        try {
          loadSoundbank(synth, new FileInputStream(macOsSoundFont));
        } catch (FileNotFoundException e) {
          throw new RuntimeException(e);
        }
      }

      if (!useQuickTimeSoundbank) {
        // if we are on windows, try to find the Quicktime 7 sound font
        try {
          // location on a 32bit system
          File windowsOsSoundFont =
              new File(
                  new File(System.getenv().getOrDefault("ProgramFiles", "C:\\Program Files")),
                  "QuickTime/QTSystem/QuickTimeMusicalInstruments.qtx");
          LOGGER.debug("Checking " + windowsOsSoundFont);
          if (!windowsOsSoundFont.exists()) {
            // loction on a 64bit system
            windowsOsSoundFont =
                new File(
                    new File(
                        System.getenv()
                            .getOrDefault("ProgramFiles(X86)", "C:\\Program Files(X86)")),
                    "QuickTime/QTSystem/QuickTimeMusicalInstruments.qtx");
            LOGGER.debug("Checking " + windowsOsSoundFont);
          }
          if (windowsOsSoundFont.exists()) {
            LOGGER.debug("Trying to load " + windowsOsSoundFont);
            InputStream stream = new FileInputStream(windowsOsSoundFont);
            // Quicktime 7 for Windows has the DTS file embedded in a DLL, so skip to it
            if (stream.skip(0x1000) != 0x1000) {
              throw new IOException("Could not skip to embedded RIFF/DTS header");
            }
            loadSoundbank(synth, stream);
          }
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }

      if (!useQuickTimeSoundbank) {
        // fall back to embedded soundbank
        loadSoundbank(synth, getClass().getResourceAsStream("/FreeFont.sf2"));
      }

      channels = synth.getChannels(); // MIDI Kanäle beginnen bei 1 !

    } catch (MidiUnavailableException e) {
      throw new RuntimeException(e);
    }
  }

  private void loadSoundbank(Synthesizer synth, InputStream stream) {
    try {
      final Soundbank soundbank = MidiSystem.getSoundbank(new BufferedInputStream(stream));
      synth.loadAllInstruments(soundbank);
      useQuickTimeSoundbank = true;
    } catch (InvalidMidiDataException | IOException e) {
      LOGGER.error("Could not load sound bank: " + e.getMessage(), e);
    }
  }

  public static synchronized MusicPlayer getMusicPlayer() {
    if (musicPlayer == null) musicPlayer = create(null);
    return musicPlayer;
  }

  public static synchronized MusicPlayer create(Synthesizer audioSynthesizer) {
    if (musicPlayer != null) throw new IllegalStateException("MusicPlayer was already created");
    else {
      musicPlayer = new MusicPlayer(audioSynthesizer);
      return musicPlayer;
    }
  }

  public boolean isUseQuickTimeSoundbank() {
    return useQuickTimeSoundbank;
  }

  public Instrument[] getInstruments() {
    return synth.getAvailableInstruments();
  }

  public void setInstrument(Instrument instrument) {
    channels[0].programChange(instrument.getPatch().getBank(), instrument.getPatch().getProgram());
  }

  public void addNotes(
      String player,
      InstrumentParameter instrument,
      int volume,
      int tempo,
      String notes,
      boolean missingParts) {
    addPlayer(player);

    Song s = this.songMap.get(player);

    if (s == null) {
      s = new Song(notes, instrument, volume, tempo, missingParts);
      this.songMap.put(player, s);
    } else {
      s.notes.append(notes);
      s.needsMore = missingParts;
    }
  }

  // returns true, when the song is complete
  public boolean isComplete() {
    // have all players their songs finished?

    String[] o = playerSet.toArray(new String[0]);

    for (String value : o) {
      Song s = songMap.get(value);
      if (s == null || s.needsMore) return false;
    }
    return true;
  }

  public void play() throws MusicException {
    // get all songs
    Object[] o = songMap.values().toArray();

    MusicQueue queue = new MusicQueue(null, 0, 0);

    for (int i = 0; i < o.length; i++) {
      Song s = (Song) o[i];
      MusicQueue q = new MusicQueue(s.instrument, i * 2, i * 2 + 1);
      q.parse(s.notes.toString(), i, s.temp, s.volume);
      queue.addAllFromMusicQueue(q);
    }

    // clear the set and map
    clear();

    playQueue(queue);
  }

  public void playQueue(MusicQueue queue) {
    stopAll();
    musicThread = new Thread(this);
    musicQueue = queue;
    running = true;
    musicThread.start();
  }

  public void stopAll() {
    running = false;
  }

  public void addPlayer(String string) {
    playerSet.add(string);
  }

  public void run() {
    Note[] notes = musicQueue.getNotes();
    int position = 0;
    for (Note note : notes) {
      if (!running) break;
      LOGGER.debug("Position: {}", position);
      // sleep for the correct amount of time
      long waitUntil = System.currentTimeMillis() + (note.position - position);

      while (System.currentTimeMillis() < waitUntil)
        try {
          Thread.sleep(1);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
      channels[note.channel].programChange(note.instrument.getMidiInstrument());
      channels[note.channel].noteOn(note.note, note.vel);
      position = note.position;
    }

    // switch all of at the end of the song
    for (MidiChannel channel : channels) channel.allNotesOff();

    musicThread = null;
    running = false;
  }

  public void clear() {
    playerSet.clear();
    songMap.clear();
  }

  public boolean isRunning() {
    return running;
  }
}

class Song {
  StringBuffer notes = new StringBuffer();

  InstrumentParameter instrument;

  int volume;

  int temp;

  boolean needsMore;

  /**
   * @param notes
   * @param instrument
   * @param volume
   * @param temp
   * @param needsMore
   */
  public Song(
      String notes, InstrumentParameter instrument, int volume, int temp, boolean needsMore) {
    super();
    this.notes.append(notes);
    this.instrument = instrument;
    this.volume = volume;
    this.temp = temp;
    this.needsMore = needsMore;
  }
}
