package net.heberling.clanlord.bard.player;

public interface MusicConstants {
  /** Number of notes in an octave */
  int OCTAVE = 12;
  /** Maximal number of octaves supported */
  int NUM_OCTAVES = 3;

  int MIDDLE_C = 60;
  int VOLUME_DEFAULT = 100;
  int TEMPO_MAX = 180;
  int TEMPO_DEFAULT = 120;
  int TEMPO_MIN = 60;
}
