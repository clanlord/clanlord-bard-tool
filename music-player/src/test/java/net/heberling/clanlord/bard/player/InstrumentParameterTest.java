package net.heberling.clanlord.bard.player;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.sound.midi.Instrument;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Patch;
import javax.sound.midi.Soundbank;
import org.junit.jupiter.api.Test;

class InstrumentParameterTest {

  @Test
  void checkAllInstrumentsAreAvailableInSoundFont() throws InvalidMidiDataException, IOException {
    InstrumentParameter[] parameters = InstrumentParameter.getAllInstrumentParameters();

    final Soundbank soundbank =
        MidiSystem.getSoundbank(
            new BufferedInputStream(
                requireNonNull(getClass().getResourceAsStream("/FreeFont.sf2"))));

    List<Integer> instrumentNumbers =
        Arrays.stream(soundbank.getInstruments())
            .map(Instrument::getPatch)
            .map(Patch::getProgram)
            .collect(Collectors.toList());

    for (InstrumentParameter parameter : parameters) {
      assertTrue(
          instrumentNumbers.contains(parameter.getMidiInstrument()),
          format("MIDI Instrument %d not found", parameter.getMidiInstrument()));
    }
  }
}
