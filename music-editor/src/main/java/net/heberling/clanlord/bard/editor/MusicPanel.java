package net.heberling.clanlord.bard.editor;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import de.tisoft.rsyntaxtextarea.parser.antlr.AntlrParserBase;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import net.heberling.clanlord.bard.parser.MusicLexer;
import net.heberling.clanlord.bard.parser.MusicParser;
import net.heberling.clanlord.bard.player.InstrumentParameter;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.TokenStream;
import org.fife.ui.rsyntaxtextarea.AbstractTokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.RSyntaxDocument;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.TokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.parser.ParseResult;
import org.fife.ui.rsyntaxtextarea.parser.ParserNotice;
import org.fife.ui.rtextarea.RTextScrollPane;

public class MusicPanel extends JPanel {

  private JComboBox<InstrumentParameter> instrumentComboBox = null;
  private JSlider tempoSlider = null;
  private JSlider volumeSlider = null;
  private RSyntaxTextArea notesTextArea = null;
  private JLabel messageLabel = null;

  /** This is the default constructor */
  public MusicPanel() {
    super();
    initialize();
  }

  /**
   * This method initializes this
   *
   * @return void
   */
  private void initialize() {
    FormLayout formLayout =
        new FormLayout(
            "3dlu,right:default,3dlu,default:grow,3dlu",
            "3dlu,pref,3dlu,pref,3dlu,pref,3dlu,top:100px:grow,3dlu,pref,3dlu");

    PanelBuilder builder = new PanelBuilder(formLayout, this);

    CellConstraints cc = new CellConstraints();

    // add Labels
    builder.addLabel("Instrument:", cc.xy(2, 2));
    builder.addLabel("Tempo:", cc.xy(2, 4));
    builder.addLabel("Volume:", cc.xy(2, 6));
    builder.addLabel("Notes:", cc.xy(2, 8));

    // add Instrument Combo Box
    instrumentComboBox = new JComboBox<>();
    DefaultComboBoxModel<InstrumentParameter> instModel = new DefaultComboBoxModel<>();

    InstrumentParameter[] instrumentParameters = InstrumentParameter.getAllInstrumentParameters();
    for (InstrumentParameter instrumentParameter : instrumentParameters) {
      instModel.addElement(instrumentParameter);
    }

    instrumentComboBox.setModel(instModel);
    builder.add(instrumentComboBox, cc.xy(4, 2));

    // add tempo slider
    tempoSlider = new JSlider();
    tempoSlider.setMinimum(60);
    tempoSlider.setMaximum(180);
    tempoSlider.setValue(120);
    tempoSlider.setPaintLabels(true);
    tempoSlider.setPaintTicks(true);
    tempoSlider.setMajorTickSpacing(20);
    tempoSlider.setMinorTickSpacing(10);
    builder.add(tempoSlider, cc.xy(4, 4));

    // add volume slider
    volumeSlider = new JSlider();
    volumeSlider.setMaximum(10);
    volumeSlider.setMinorTickSpacing(1);
    volumeSlider.setPaintLabels(true);
    volumeSlider.setPaintTicks(true);
    volumeSlider.setMajorTickSpacing(2);
    volumeSlider.setMinimum(0);
    builder.add(volumeSlider, cc.xy(4, 6));

    AbstractTokenMakerFactory atmf =
        (AbstractTokenMakerFactory) TokenMakerFactory.getDefaultInstance();
    atmf.putMapping(
        "text/clmusic", MusicTokenMaker.class.getName(), MusicTokenMaker.class.getClassLoader());
    // add notes text area
    notesTextArea = new RSyntaxTextArea();
    notesTextArea.setSyntaxEditingStyle("text/clmusic");
    notesTextArea.addParser(
        new AntlrParserBase<MusicLexer, MusicParser>() {
          @Override
          protected MusicLexer createLexer(CharStream charStream) {
            return new MusicLexer(charStream);
          }

          @Override
          protected MusicParser createParser(TokenStream tokenStream) {
            return new MusicParser(tokenStream);
          }

          @Override
          protected void parse(MusicParser musicParser) {
            musicParser.song();
          }

          @Override
          public ParseResult parse(RSyntaxDocument doc, String style) {
            ParseResult parseResult = super.parse(doc, style);
            messageLabel.setText(null);
            parseResult.getNotices().stream()
                .map(ParserNotice::getMessage)
                .findFirst()
                .ifPresent(messageLabel::setText);

            return parseResult;
          }
        });
    RTextScrollPane scrollPane = new RTextScrollPane(notesTextArea);
    builder.add(scrollPane, cc.xy(4, 8, "fill,fill"));

    // add status massage label
    messageLabel = new JLabel("OK");
    builder.add(messageLabel, cc.xyw(2, 10, 3, "fill,default"));
  }

  public int getVolume() {
    return volumeSlider.getValue();
  }

  public int getTempo() {
    return tempoSlider.getValue();
  }

  public InstrumentParameter getInstrument() {
    return (InstrumentParameter) instrumentComboBox.getSelectedItem();
  }

  public String getNotes() {
    return notesTextArea.getText();
  }

  public void setNoteCursor(int position) {
    if (position >= 0) {
      notesTextArea.requestFocusInWindow();
      notesTextArea.setCaretPosition(position);
      notesTextArea.setSelectionStart(position);
      notesTextArea.setSelectionEnd(position);
    }
  }

  public void setMessage(String message) {
    messageLabel.setText(message);
  }
}
