package net.heberling.clanlord.bard.editor;

import de.tisoft.rsyntaxtextarea.modes.antlr.AntlrTokenMaker;
import de.tisoft.rsyntaxtextarea.modes.antlr.MultiLineTokenInfo;
import net.heberling.clanlord.bard.parser.MusicLexer;
import org.antlr.v4.runtime.CharStreams;
import org.fife.ui.rsyntaxtextarea.Token;

public final class MusicTokenMaker extends AntlrTokenMaker {
  public MusicTokenMaker() {
    super(new MultiLineTokenInfo(0, Token.COMMENT_MULTILINE, "<", ">"));
  }

  @Override
  protected int convertType(int type) {
    switch (type) {
      case MusicLexer.ML_COMMENT:
        return Token.COMMENT_MULTILINE;
      case MusicLexer.NOTELETTER:
        return Token.RESERVED_WORD;
      case MusicLexer.P:
        return Token.RESERVED_WORD_2;
      case MusicLexer.INTEGER:
        return Token.LITERAL_NUMBER_DECIMAL_INT;
        // sharps and flats
      case MusicLexer.HASH:
      case MusicLexer.PERIOD:
        return Token.OPERATOR;
        // octaves
      case MusicLexer.BACKSLASH:
      case MusicLexer.PLUS:
      case MusicLexer.EQ:
      case MusicLexer.MINUS:
      case MusicLexer.SLASH:
        return Token.FUNCTION;
        // loops
      case MusicLexer.OPENBR:
      case MusicLexer.BAR:
      case MusicLexer.CLOSEBR:
        // chords
      case MusicLexer.OPENSQ:
      case MusicLexer.CLOSESQ:
      case MusicLexer.DOLLAR:
        return Token.SEPARATOR;
        // temp
      case MusicLexer.AT:
      case MusicLexer.ATEQ:
      case MusicLexer.ATMINUS:
      case MusicLexer.ATPLUS:
        return Token.VARIABLE;
        // volume
      case MusicLexer.PERCENT:
      case MusicLexer.OPENCR:
      case MusicLexer.CLOSECR:
        return Token.IDENTIFIER;
    }
    return type;
  }

  @Override
  protected MusicLexer createLexer(String text) {
    return new MusicLexer(CharStreams.fromString(text));
  }
}
