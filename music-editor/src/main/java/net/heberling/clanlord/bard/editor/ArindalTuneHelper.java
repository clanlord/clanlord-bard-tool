package net.heberling.clanlord.bard.editor;

import static javax.swing.JComponent.WHEN_IN_FOCUSED_WINDOW;

import com.jgoodies.forms.builder.ButtonBarBuilder;
import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import net.heberling.clanlord.bard.player.InstrumentParameter;
import net.heberling.clanlord.bard.player.MusicException;
import net.heberling.clanlord.bard.player.MusicPlayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ArindalTuneHelper extends JFrame {
  private static final Logger LOGGER = LoggerFactory.getLogger(ArindalTuneHelper.class);
  private static final int MAX_BAND_SIZE = 3;

  private static final int MASK = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

  private Action playButton = null;

  private Action playAllButton = null;

  private Action quitButton = null;

  private final MusicPlayer musicPlayer;

  private Action cancelButton = null;

  private MusicPanel[] musicPanel = null;

  private JTabbedPane tabbedPane = null;

  private static final String VERSION = readVersion();

  /** This is the default constructor */
  public ArindalTuneHelper() {
    super();
    musicPlayer = MusicPlayer.getMusicPlayer();
    initialize();
  }

  private static String readVersion() {
    return ArindalTuneHelper.class.getPackage().getImplementationVersion();
  }

  /** This method initializes this */
  private void initialize() {
    JPanel jcontentpane = new JPanel();
    jcontentpane.setLayout(new BorderLayout());

    tabbedPane = new JTabbedPane();

    musicPanel = new MusicPanel[MAX_BAND_SIZE];
    for (int i = 0; i < musicPanel.length; i++) {
      musicPanel[i] = new MusicPanel();
      tabbedPane.add("Instrument " + (i + 1), musicPanel[i]);
      if (!musicPlayer.isUseQuickTimeSoundbank()) {
        musicPanel[i].setMessage(
            "Could not find QuickTime. Instruments will sound slightly different.");
      }
    }

    jcontentpane.add(tabbedPane, BorderLayout.CENTER);

    ButtonBarBuilder builder = new ButtonBarBuilder();
    builder.addButton(getPlayButton(), getPlayAllButton(), getCancelButton());
    builder.addGlue();
    builder.addButton(getQuitButton());

    jcontentpane.add(builder.getPanel(), BorderLayout.SOUTH);

    this.setContentPane(jcontentpane);
    this.setTitle("Arindal Bard Tool " + VERSION + " by AtoroGM");
    // this.setSize(new Dimension(559, 323));
    this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    updateKeyBinding();
    this.pack();
  }

  private Action getPlayButton() {
    if (playButton == null) {
      playButton =
          new AbstractAction("Play") {
            @Override
            public void actionPerformed(ActionEvent e) {
              new Thread(() -> play(musicPanel[tabbedPane.getSelectedIndex()])).start();
            }
          };
      getRootPane().getActionMap().put(playButton, playButton);
    }
    return playButton;
  }

  private Action getPlayAllButton() {
    if (playAllButton == null) {
      playAllButton =
          new AbstractAction("Play All") {
            @Override
            public void actionPerformed(ActionEvent e) {
              new Thread(() -> play(musicPanel)).start();
            }
          };
      getRootPane().getActionMap().put(playAllButton, playAllButton);
    }
    return playAllButton;
  }

  private void updateKeyBinding() {
    if (cancelButton.isEnabled()) {
      getRootPane()
          .getInputMap(WHEN_IN_FOCUSED_WINDOW)
          .put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, KeyEvent.CTRL_DOWN_MASK), cancelButton);
      getRootPane()
          .getInputMap(WHEN_IN_FOCUSED_WINDOW)
          .put(
              KeyStroke.getKeyStroke(
                  KeyEvent.VK_SPACE, KeyEvent.CTRL_DOWN_MASK | KeyEvent.SHIFT_DOWN_MASK),
              cancelButton);
    } else {
      getRootPane()
          .getInputMap(WHEN_IN_FOCUSED_WINDOW)
          .put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, KeyEvent.CTRL_DOWN_MASK), playButton);
      getRootPane()
          .getInputMap(WHEN_IN_FOCUSED_WINDOW)
          .put(
              KeyStroke.getKeyStroke(
                  KeyEvent.VK_SPACE, KeyEvent.CTRL_DOWN_MASK | KeyEvent.SHIFT_DOWN_MASK),
              playAllButton);
    }
  }

  private void play(MusicPanel... musicPanel) {
    try {
      playButton.setEnabled(false);
      playAllButton.setEnabled(false);
      cancelButton.setEnabled(true);
      updateKeyBinding();
      musicPlayer.clear();
      for (int i = 0; i < musicPanel.length; i++) {
        MusicPanel currentMusicPanel = musicPanel[i];
        currentMusicPanel.setMessage("");
        InstrumentParameter ip = currentMusicPanel.getInstrument();
        int tempo = currentMusicPanel.getTempo();
        int volume = currentMusicPanel.getVolume() * 10;

        musicPlayer.addNotes("player_" + i, ip, volume, tempo, currentMusicPanel.getNotes(), false);
      }
      musicPlayer.play();
      while (musicPlayer.isRunning()) {
        Thread.yield();
      }
      // musicPanel.setMessage("Finished.");
    } catch (MusicException e1) {
      LOGGER.debug(e1.getPosition() + " " + e1.getError());
      tabbedPane.setSelectedComponent(musicPanel[e1.getChannel()]);
      musicPanel[e1.getChannel()].setNoteCursor(e1.getPosition());
      musicPanel[e1.getChannel()].setMessage(e1.getMessage());
      musicPlayer.stopAll();
      musicPlayer.clear();
    } finally {
      cancelButton.setEnabled(false);
      playButton.setEnabled(true);
      playAllButton.setEnabled(true);
      updateKeyBinding();
    }
  }

  /**
   * This method initializes jButton1
   *
   * @return javax.swing.JButton
   */
  private Action getQuitButton() {
    if (quitButton == null) {
      quitButton =
          new AbstractAction("Quit") {
            @Override
            public void actionPerformed(ActionEvent e) {
              System.exit(0);
            }
          };
    }
    return quitButton;
  }

  /**
   * This method initializes cancelButton
   *
   * @return javax.swing.JButton
   */
  private Action getCancelButton() {
    if (cancelButton == null) {
      cancelButton =
          new AbstractAction("Cancel") {
            @Override
            public void actionPerformed(ActionEvent e) {
              musicPlayer.stopAll();
              musicPlayer.clear();
            }
          };
      cancelButton.setEnabled(false);
      getRootPane().getActionMap().put(cancelButton, cancelButton);
    }
    return cancelButton;
  }

  public static void main(String[] args) {
    new ArindalTuneHelper().setVisible(true);
  }
}
